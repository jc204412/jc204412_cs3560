//board_slot class

#ifndef BOARD_SLOT_H
#define BOARD_SLOT_H

class board_slot{
    public:
        enum tile_status{
            EMPTY,
            BLACK,
            WHITE,
        };
        board_slot();
        void flip();
        bool isHighLighted;
        bool isPossible;
        void set_status(tile_status newStatus);
        tile_status get_status() const;
        
    private:
        tile_status current_state;
};

#endif
