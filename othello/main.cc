/*
Phillip Cook, Othello project
CS2401 Summer 2014
*/

#include <iostream>
#include <ncurses.h>

#include "othello.h"
#include "game.h"

using namespace std;

int main(){
    Othello O;
    
    main_savitch_14::game::who winner;
    
    initscr();//using this to start ncurses mode
    start_color(); //enble colors in ncurses
    noecho();
    cbreak(); //disable line buffering
    keypad(stdscr, true); //enable use of arrow and function keys
    
    winner = O.play();
    init_pair(7, COLOR_WHITE, COLOR_BLACK);
    init_pair(5, COLOR_BLACK, COLOR_BLACK);
    attron(COLOR_PAIR(5));
    move(0, 0);
    hline('-', 80);
    attron(COLOR_PAIR(7));
    if(winner == main_savitch_14::game::HUMAN) mvprintw(0, 0, "Black Wins!");
    if(winner == main_savitch_14::game::COMPUTER) mvprintw(0, 0, "White Wins!");
    if(winner == main_savitch_14::game::NEUTRAL) mvprintw(0, 0, "It's a tie!");
    
    getch(); //wait for input to prevent fast exit
    endwin(); //close ncurses window
    
    return 0;
}
