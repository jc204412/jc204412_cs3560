//othello.cc

#include "othello.h"

Othello::Othello(){
    //starting positions
    board[3][3].set_status(board_slot::WHITE); // top left center
    board[4][4].set_status(board_slot::WHITE); // bot right center
    board[3][4].set_status(board_slot::BLACK); // top right center
    board[4][3].set_status(board_slot::BLACK); // bot left center
    
    playerPassed = false;
    gameOver = false;
}

Othello::Othello(const Othello& other){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            board[i][j] = other.board[i][j];
        }
    }
    playerPassed = other.playerPassed;
    gameOver = other.gameOver;
}

main_savitch_14::game* Othello::clone() const{
    main_savitch_14::game* clone = new Othello(*this);
    return clone;
}

void Othello::compute_moves(std::queue<Move>& moves){
    //find all available moves from the board for a player.
    Move tmp;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            tmp.row = i;
            tmp.col = j;
            if(is_legal(tmp)){
                moves.push(tmp);
                board[i][j].isPossible = true;
            }
        }
    }
}

int Othello::evaluate() const{
    //returns negative for next mover and positive for last mover
    //HUMAN is black
    int black = 0;
    int white = 0;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if (board[i][j].get_status() == board_slot::BLACK) black++;
            if (board[i][j].get_status() == board_slot::WHITE) white++;
        }
    }
    if(next_mover() == HUMAN) return (white - black);
    else return (black - white);
}

void Othello::display_status() const{
    const unsigned int TILE_WIDTH = 10;
    const unsigned int TILE_HEIGHT = 5;
    const unsigned int BOARD_TOP = 2;
    const unsigned int BOARD_LEFT = 1;
    
    unsigned int crow, ccol;
    
    init_pair(1, COLOR_WHITE, COLOR_GREEN); //for green grid
    init_pair(2, COLOR_WHITE, COLOR_BLUE); // for highlighted
    init_pair(3, COLOR_WHITE, COLOR_YELLOW); // for possible
    init_pair(4, COLOR_WHITE, COLOR_WHITE);
    init_pair(5, COLOR_BLACK, COLOR_BLACK);
    init_pair(6, COLOR_GREEN, COLOR_GREEN);
    
    for(int row = 0; row < 8; row++){
        for(int col = 0; col < 8; col++){
            //print each tile
            crow = row*TILE_HEIGHT + BOARD_TOP;
            ccol = col*TILE_WIDTH + BOARD_LEFT;
            move(crow, ccol);
            
            attron(COLOR_PAIR(1)); //set grid color
            if(board[row][col].isHighLighted) attron(COLOR_PAIR(2));
            else if(board[row][col].isPossible) attron(COLOR_PAIR(3));
            //draw box
            vline('|', TILE_HEIGHT - 1);
            move(row*TILE_HEIGHT + BOARD_TOP, (col + 1)*TILE_WIDTH + BOARD_LEFT - 1); //top right of tile
            vline('|', TILE_HEIGHT - 1);
            move(row*TILE_HEIGHT + BOARD_TOP, col*TILE_WIDTH + BOARD_LEFT);
            hline('-', TILE_WIDTH);
            move((row + 1)*TILE_HEIGHT + BOARD_TOP - 1, col*TILE_WIDTH + BOARD_LEFT); //bottem left of tile
            hline('-', TILE_WIDTH);
            
            attron(COLOR_PAIR(6));
            for(int i = 1; i < 4; i++){
                move(crow + i, ccol + 1);
                hline(ACS_BLOCK, 8);
            }
            //draw a game piece!!
            if(board[row][col].get_status() == board_slot::BLACK){
                attron(COLOR_PAIR(5));
            }
            if(board[row][col].get_status() == board_slot::WHITE){
                attron(COLOR_PAIR(4));
            }
            if(board[row][col].get_status() == board_slot::EMPTY) attron(COLOR_PAIR(1));
            else {
                move(crow + 1, ccol + 3);
                hline(ACS_BLOCK, 4);
                move(crow + 2, ccol + 2);
                hline(ACS_BLOCK, 6);
                move(crow + 3, ccol + 3);
                hline(ACS_BLOCK, 4);
            }
        }
    }
    refresh();//from ncurses
}

Move Othello::get_user_move(){
    clear_possible();
    clear_highlight();
    init_pair(7, COLOR_WHITE, COLOR_BLACK);
    attron(COLOR_PAIR(7));
    mvprintw(0,0, "To make a move use the right arrow key to selct a highlighted possible move");
    bool cursor = false; //needed to blink the selected tile.
    bool submit = false; //goes to true when user selects a valid move
    int input;
    Move selected_move;
    std::queue<Move> possible_moves;
    compute_moves(possible_moves); //fill queue with all the possible moves
    if(possible_moves.empty()){
        selected_move.row = -1;
        selected_move.col = -1;
        return selected_move;
    }
    playerPassed = false;
    selected_move = possible_moves.front();
    display_status();
    while(!submit){
        input = getch(); //take in keyboard input to change selection
        move(0,0);
        switch(input){
            case KEY_LEFT:
            case 68:
                //selected_move = possible_moves.back();
                
            break;
            case KEY_RIGHT:
            case 67:
                possible_moves.pop();
                possible_moves.push(selected_move);
                selected_move = possible_moves.front();
            break;
            case 10: //Enter key
                submit = true;
            break;
            default:
                //do nothing.
            break;
        }
        //clear highlight
        clear_highlight();
        //highlight selected move
        board[selected_move.row][selected_move.col].isHighLighted = true;
        //update display
        display_status();
        
    }
    //remember to unset possible moves.
    clear_possible();
    return selected_move;
}

bool Othello::is_legal(const Move& move) const{
    if(move.row < 0) return true; //passing move
    if(board[move.row][move.col].get_status() != board_slot::EMPTY) return false; // this move is taken
    
    board_slot::tile_status current_color;
    board_slot::tile_status other_color;
    //bool isAdjacent;
    
    //need to know who's turn it is.
    if(next_mover() == HUMAN){
        current_color = board_slot::BLACK; //should be black's turn
        other_color = board_slot::WHITE;
    }
    else{
        current_color = board_slot::WHITE;
        other_color = board_slot::BLACK;
    }
    //must be adjacent to opposite color
    for(int i = -1; i < 2; i++){
        if(move.row + i >= 0 && move.row + i < 8)
        for(int j = -1; j < 2; j++){
            if(move.col + j >= 0 && move.col + j < 8){
                if(i == 0 && j == 0); //donothing
                else if(board[move.row + i][move.col + j].get_status() == other_color){
                    //look for paired piece in direction of adjacency
                    for(int k = 2; k < 8; k++){
                        if(move.row + k*i < 0 || move.row + k*i > 7 || move.col + k*j < 0 || move.col + k*j > 7); //do nothing.
                        else if(board[move.row + i*k][move.col + j*k].get_status() == board_slot::EMPTY) break;//exit for loop
                        else if(board[move.row + i*k][move.col + j*k].get_status() == current_color){
                            //this is a legal move.
                            return true;
                        }
                    }
                }
            }
        }
    }
    //not a legal move.
    return false;
}

void Othello::make_move(const Move& move){
    if(move.row < 0){ //pass detected
        if(playerPassed) gameOver = true;
        else playerPassed = true;        
        main_savitch_14::game::make_move(move);
        return;
    }
    
    board_slot::tile_status current_color;
    board_slot::tile_status other_color;
    
    //who's turn?
    if(next_mover() == HUMAN){
        current_color = board_slot::BLACK; //should be black's turn
        other_color = board_slot::WHITE;
    }
    else{
        current_color = board_slot::WHITE;
        other_color = board_slot::BLACK;
    }
    board[move.row][move.col].set_status(current_color);//place selected piece
    //find lines of capture.
    for(int i = -1; i < 2; i++){
        if(move.row + i >= 0 && move.row + i < 8)
        for(int j = -1; j < 2; j++){
            if(move.col + j >= 0 && move.col + j < 8){
                if(i == 0 && j == 0); //donothing
                else if(board[move.row + i][move.col + j].get_status() == other_color){
                    //look for paired piece in direction of adjacency
                    for(int k = 2; k < 8; k++){
                        if(move.row + k*i < 0 || move.row + k*i > 7 || move.col + k*j < 0 || move.col + k*j > 7); //do nothing.
                        else if(board[move.row + i*k][move.col + j*k].get_status() == board_slot::EMPTY) break;//exit for loop
                        else if(board[move.row + i*k][move.col + j*k].get_status() == current_color){
                            //flip all pieces from here back
                            for(int n = k - 1; n > 0; n--){
                                board[move.row + i*n][move.col + j*n].flip();
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
    playerPassed = false;
    main_savitch_14::game::make_move(move);
}

bool Othello::is_game_over() const{
    //game is over if BOTH players have no moves.
    
    return (gameOver);
};

void Othello::clear_possible(){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            board[i][j].isPossible = false;
        }
    }
}

void Othello::clear_highlight(){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            board[i][j].isHighLighted = false;
        }
    }
}
