//Othello class

#ifndef OTHELLO_H
#define OTHELLO_H

#include "game.h"
#include "board.h"
#include "move.h"
#include <ncurses.h>

class Othello:public main_savitch_14::game{
    public:
        Othello();
        Othello(const Othello& other);
    
        game* clone() const;
        void compute_moves(std::queue<Move>& moves);
        Move get_user_move();
        void make_move(const Move& move);
        void display_status() const;
        int evaluate() const;
        bool is_game_over() const;
        bool is_legal(const Move& move) const;
        void restart(){};
    private:
        board_slot board[8][8];
        bool playerPassed;
        bool gameOver;
        void clear_possible();
        void clear_highlight();
};

#endif
