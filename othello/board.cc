//board.cc

#include "board.h"

board_slot::board_slot(){
    current_state = EMPTY;
    isHighLighted = false;
    isPossible = false;
}

void board_slot::flip(){
    if(current_state == EMPTY) return; //hmm... hopefullty this doesn't happen
    if(current_state == BLACK) current_state = WHITE;
    else current_state = BLACK;
}

void board_slot::set_status(tile_status newStatus){
    current_state = newStatus;
}

board_slot::tile_status board_slot::get_status() const{
    return current_state;
}
