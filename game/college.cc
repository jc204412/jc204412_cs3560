/*
	Implementation file for the College container
	CS 2401 Project 3
	Spring 2014/15
	Jacob Carlsen
*/

#include <iostream>
#include <string>
#include <fstream>
#include "node.h"
#include "course.h"
#include "college.h"

College::College(){
	name = "A" ;
	head = NULL ;
}

College::College(const College& other){
	if(other.head == NULL){head = NULL;}
	else{
		head = new node(other.head->data()) ;
		node* sptr = other.head->link() ;
		node* dptr = head ;
		while(sptr != NULL){
			dptr->set_link(new node (sptr->data())) ;
			dptr = dptr->link() ;
			sptr = sptr->link() ;
		}
	}
}



College::College(std::string user){
	name = user ;
	head = NULL ;
}

College::~College(){
	node* temp ;
	while(head != NULL){
		temp = head->link() ;
		delete head;
		head = temp;
	}
}

College College::acopy(const College& original){
	if(original.head !=NULL){
		College duplicate(original.name) ;
		node* cursor = head ;
		while(cursor != NULL){
			duplicate.add(cursor->data());
			cursor = cursor->link() ;
		}
        return duplicate;
	}
	else{
		std::cout <<"List empty!" ;
		College empty ;
		return(empty) ;
	}
}

College College::operator =(const College& other){
	if(this != &other){
		node* temp = head ;
		while(head != NULL){
			head = head->link() ;
			delete temp ;
		}
		name = other.name ;
		temp = other.head->link() ;
		while(temp != NULL){
			add(temp->data()) ;
			temp = temp->link() ;
		}
        return *this;
	}
	else{
		return(*this) ;
	}
}

void College::load(std::ifstream& fin){
	course temp ;
	while (fin >> temp){
		add(temp) ;
		fin.ignore(10000,'\n');
	}
}

void College::save(std::ostream& outs){
	node* cursor = head ;
	while (cursor != NULL){
		outs << cursor->data() ;
		cursor = cursor->link() ;
	}
}


void College::add(course c){
	node* cursor = head ;
	if(head == NULL){
		head = new node(c, NULL) ;
	}
	else{
		while(cursor->link() != NULL && cursor->link()->data().get_course_number() < c.get_course_number())
		{
			cursor = cursor->link() ;
		}
		node* temp = new node(c, cursor->link()) ;
		cursor->set_link(temp) ;
	}
}

void College::display(std::ostream& outs){
	node* cursor = head ;
	while(cursor != NULL){
		cursor->data().output(outs) ;
		cursor = cursor->link() ;
	}	
}

void College::remove(std::string cname){
	if(head == NULL){
		std::cout<<"Empty List!!" <<std::endl ;
	}
	else if(head->data().get_course_number() == cname){
		node* temp ;
		temp = head ;
		head = head->link() ;
		delete temp ;
	}
	else{
		node* cursor ;
		node* previous ;
		cursor = head ;
        previous = head;
		while(cursor != NULL && cursor->data().get_course_number() != cname){
			previous = cursor ;
			cursor = cursor ->link() ;
		}
		if(cursor != NULL){
			previous->set_link(cursor->link()) ;
			delete cursor ;
		}
		else{
			std::cout <<"Not in list!!!\n" ;
		}
	}
}	

double College::hours(){
	node* cursor = head ;
	double hours_sum = 0.0 ;
	while(cursor != NULL){
		hours_sum = hours_sum + cursor->data().get_hours() ;
		cursor = cursor ->link() ;
	}
	return (hours_sum) ;
}

double College::gpa(){
	node* cursor = head ;
	double grade = 0.0 ;
	while(cursor != NULL){
		grade = grade +(cursor->data().get_hours() * cursor->data().get_number_grade()) ;
		cursor = cursor ->link() ;
	}
	double gpa ;
	gpa = grade / hours() ;

	return (gpa) ;
}













