/*
	College container declaration file
	CS 2401 Project 3 
	Spring 2014/15
	Jacob Carlsen
*/

#include <iostream>
#include <fstream>
#include <string>
#include "node.h"
#include "course.h"
#ifndef COLLEGE_H
#define COLLEGE_H

class College{
  public:	 
	College() ;
	College(const College& other) ;
	College(std::string user) ;
	~College() ;
	College acopy(const College& original) ;
	College operator =(const College& other) ; 
	void load(std::ifstream& fin) ;
	void save(std::ostream& outs) ;
	void add(course c) ;
	void display(std::ostream& outs) ;
	void remove(std::string coursename) ;
	double hours() ;
	double gpa() ;
	
  private:
	std::string name ;
	node* head ;
};

#endif
